#import <QuickLook/QuickLook.h>

@interface GSRootController : UINavigationController <QLPreviewControllerDataSource> {
  dispatch_source_t _directorySource;
  NSURL* _directoryURL;
  QLPreviewController* _previewController;
  NSArray* _previewURLs;
}
-(BOOL)handleURL:(NSURL*)URL;
@end
