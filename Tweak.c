#include <CoreFoundation/CoreFoundation.h>
#include <CoreFoundation/CFUserNotification.h>
#include <CoreGraphics/CoreGraphics.h>
#include <cups/cups.h>
#include <dispatch/dispatch.h>
#include <mach-o/dyld.h>
#include <substrate.h>

static int (*_LSStartOpenOperation)(CFURLRef resourceURL,CFStringRef applicationIdentifier,CFStringRef documentIdentifier,CFDictionaryRef userInfo,CFDictionaryRef options);
static int (*_LSFinishOpenOperation)(CFURLRef resourceURL);

struct printd_transaction {
  int index;
  http_t* http;
  ipp_t* request;
  ipp_t* response;
  time_t tstamp;
};

static int (*F_writeResponse)(struct printd_transaction* transaction,int HTTPCode,char* MIMEType,long long contentLength);
static int f_writeResponse(struct printd_transaction* transaction,int HTTPCode,char* MIMEType,long long contentLength) {
  if(HTTPCode==200 && ippGetOperation(transaction->request)==IPP_OP_GET_PRINTER_ATTRIBUTES){
    ipp_t* response=transaction->response;
    const char* const ckey="com.apple.printing.last-used-printers-col";
    ipp_attribute_t* cattr=ippFindAttribute(response,ckey,IPP_TAG_BEGIN_COLLECTION);
    const char* const key="printer-dns-sd-name";
    const char* const value="ipp://%2Fvar%2Frun%2Fprintd/";
    const ipp_tag_t vtype=IPP_TAG_NAME;
    const char* name;
    if(!cattr || !((name=ippGetString(ippFindAttribute(ippGetCollection(cattr,0),
     key,vtype),0,NULL))) || strcmp(name,value)){
      const int count=ippGetCount(cattr)+1;
      ipp_t** items=malloc(count*sizeof(ipp_t*));
      ippAddString(items[0]=ippNew(),IPP_TAG_ZERO,vtype,key,NULL,value);
      for (int i=1;i<count;i++){items[i]=ippGetCollection(cattr,i-1);}
      ippDeleteAttribute(response,cattr);
      ippAddCollections(response,IPP_TAG_PRINTER,ckey,count,(const ipp_t**)items);
      ippDelete(items[0]);
      free(items);
      contentLength=ippLength(response);
    }
  }
  return F_writeResponse(transaction,HTTPCode,MIMEType,contentLength);
}
static int (*F_processIPPRequest)(struct printd_transaction* transaction);
static int f_processIPPRequest(struct printd_transaction* transaction) {
  ipp_t* request=transaction->request;
  const char* uri=ippGetString(ippFindAttribute(request,"printer-uri",IPP_TAG_URI),0,NULL);
  const char* const prefix="ipp://%2F";
  if(uri && strncmp(uri,prefix,strlen(prefix))==0){
    ipp_t* response=ippNewResponse(request);
    const ipp_op_t operation=ippGetOperation(request);
    if(operation==IPP_OP_PRINT_JOB){
      http_t* http=transaction->http;
      if((httpGetExpect(http)==100 && !F_writeResponse(transaction,100,NULL,0))
       || ippGetState(request)!=IPP_STATE_DATA){
        ippDelete(response);
        return F_writeResponse(transaction,400,NULL,0);
      }
      const char* const tmpdir=getenv("TMPDIR")?:"/tmp";
      const char* const tmpfn_prefix="/print-";
      const char* const tmpfn_suffix=".pdf";
      const size_t xlen=6;
      const size_t tmpdirlen=strlen(tmpdir);
      const size_t tmpfn_prelen=strlen(tmpfn_prefix);
      const size_t tmpfn_suflen=strlen(tmpfn_suffix);
      const size_t tmpfn_len=tmpdirlen+tmpfn_prelen+xlen+tmpfn_suflen;
      char* tmpfn=malloc(tmpfn_len+1);
      char* xptr=memset(memcpy(memcpy(tmpfn,tmpdir,tmpdirlen)+tmpdirlen,
       tmpfn_prefix,tmpfn_prelen)+tmpfn_prelen,'X',xlen);
      strcpy(xptr+xlen,tmpfn_suffix);
      int tmpfd=mkstemps(tmpfn,tmpfn_suflen);
      char buf[1<<12];
      ssize_t len;
      while((len=httpRead2(http,buf,sizeof(buf)))>0){write(tmpfd,buf,len);}
      close(tmpfd);
      ipp_attribute_t* attr=ippFindAttribute(request,"page-ranges",IPP_TAG_RANGE);
      int npages=attr?2*ippGetCount(attr):0;
      int* pages;
      if(npages){
        pages=malloc(npages*sizeof(*pages));
        for (int i=0;i<npages;i+=2){pages[i]=ippGetRange(attr,i/2,&pages[i+1]);}
      }
      else {pages=NULL;}
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0),^{
        if(pages){
          CGDataProviderRef provider=CGDataProviderCreateWithFilename(tmpfn);
          CGPDFDocumentRef pdf=CGPDFDocumentCreateWithProvider(provider);
          CGDataProviderRelease(provider);
          unlink(tmpfn);
          memset(xptr,'X',xlen);
          CGDataConsumerRef consumer=CGDataConsumerCreate(
           (void*)(intptr_t)mkstemps(tmpfn,tmpfn_suflen),&(CGDataConsumerCallbacks){
           .putBytes=(CGDataConsumerPutBytesCallback)write,
           .releaseConsumer=(CGDataConsumerReleaseInfoCallback)close});
          CGContextRef context=CGPDFContextCreate(consumer,NULL,NULL);
          CGDataConsumerRelease(consumer);
          for (int i=0;i<npages;i+=2){
            const int nmax=pages[i+1];
            for (int n=pages[i];n<=nmax;n++){
              CGPDFPageRef page=CGPDFDocumentGetPage(pdf,n);
              if(page){
                CGRect box=CGPDFPageGetBoxRect(page,kCGPDFMediaBox);
                CGContextBeginPage(context,&box);
                CGContextDrawPDFPage(context,page);
                CGContextEndPage(context);
              }
            }
          }
          CGPDFContextClose(context);
          CGContextRelease(context);
          CGPDFDocumentRelease(pdf);
          free(pages);
        }
        CFStringRef appID=CFPreferencesCopyAppValue(
         CFSTR("PDFPrinterOpenInApp"),kCFPreferencesCurrentApplication)?:
         CFSTR("com.officialscheduler.GSPrint");
        CFURLRef URL=CFURLCreateFromFileSystemRepresentation(
         NULL,(const UInt8*)tmpfn,tmpfn_len,false);
        int ecode=_LSStartOpenOperation(URL,appID,NULL,NULL,NULL);
        if(ecode==36){ecode=_LSFinishOpenOperation(URL);}
        CFRelease(URL);
        if(ecode){
          CFStringRef message=CFStringCreateWithFormat(NULL,NULL,
           CFSTR("LSOpenOperation error %d.\nFile: %s\nAppID: %@"),ecode,tmpfn,appID);
          CFUserNotificationDisplayNotice(0,kCFUserNotificationCautionAlertLevel,
           NULL,NULL,NULL,CFSTR("PDF Printer"),message,NULL);
          CFRelease(message);
        }
        else {unlink(tmpfn);}
        CFRelease(appID);
        free(tmpfn);
      });
      static int jobIndex=0;
      ippAddInteger(response,IPP_TAG_JOB,IPP_TAG_INTEGER,"job-id",++jobIndex);
      ippAddInteger(response,IPP_TAG_JOB,IPP_TAG_ENUM,"job-state",IPP_JSTATE_COMPLETED);
      ippAddString(response,IPP_TAG_JOB,IPP_TAG_KEYWORD,
       "job-state-reasons",NULL,"completed-successfully");
    }
    else if(operation==IPP_OP_GET_PRINTER_ATTRIBUTES){
      ipp_attribute_t* attr=ippFindAttribute(request,"requested-attributes",IPP_TAG_KEYWORD);
      const int count=ippGetCount(attr);
      for (int i=0;i<count;i++){
        const char* name=ippGetString(attr,i,NULL);
        const char* key;
        if(strcmp(name,key="compression-supported")==0
         || strcmp(name,key="print-scaling-supported")==0
         || strcmp(name,key="printer-state-reasons")==0)
          ippAddString(response,IPP_TAG_PRINTER,IPP_TAG_KEYWORD,key,NULL,"none");
        else if(strcmp(name,key="document-format-supported")==0)
          ippAddStrings(response,IPP_TAG_PRINTER,IPP_TAG_MIMETYPE,key,2,NULL,
          (const char* const[]){CUPS_FORMAT_PDF,"image/urf"});
        else if(strcmp(name,key="media-col-database")==0){
          CFStringRef mediaName=CFPreferencesCopyAppValue(
           CFSTR("PDFPrinterMediaSize"),kCFPreferencesCurrentApplication);
          const pwg_media_t* media=NULL;
          if(mediaName){
            const CFStringEncoding encoding=kCFStringEncodingMacRoman;
            const char* name=CFStringGetCStringPtr(mediaName,encoding);
            UInt8* namebuf=NULL;
            if(!name){
              CFIndex namelen;
              CFRange range=CFRangeMake(0,CFStringGetLength(mediaName));
              if(CFStringGetBytes(mediaName,range,encoding,0,false,NULL,0,&namelen)
               && CFStringGetBytes(mediaName,range,encoding,0,false,
               namebuf=malloc(namelen+1),namelen,&namelen)){
                namebuf[namelen]=0;
                name=(const char*)namebuf;
              }
            }
            CFRelease(mediaName);
            if(name){media=pwgMediaForPPD(name);}
            if(namebuf){free(namebuf);}
          }
          size_t count,i;
          if(media){count=1;}
          else {
            extern const pwg_media_t* _pwgMediaTable(size_t*);
            media=_pwgMediaTable(&count);
          }
          ipp_t** items=malloc(count*sizeof(ipp_t*));
          for (i=0;i<count;i++){
            ipp_t* mcol=ippNew();
            ippAddInteger(mcol,IPP_TAG_ZERO,IPP_TAG_INTEGER,"x-dimension",media[i].width);
            ippAddInteger(mcol,IPP_TAG_ZERO,IPP_TAG_INTEGER,"y-dimension",media[i].length);
            ippAddCollection(items[i]=ippNew(),IPP_TAG_ZERO,"media-size",mcol);
            ippDelete(mcol);
          }
          ippAddCollections(response,IPP_TAG_PRINTER,key,count,(const ipp_t**)items);
          for (i=0;i<count;i++){ippDelete(items[i]);}
          free(items);
        }
        else if(strcmp(name,key="media-col-supported")==0)
          ippAddString(response,IPP_TAG_PRINTER,IPP_TAG_KEYWORD,key,NULL,"media-size");
        else if(strcmp(name,key="operations-supported")==0)
          ippAddIntegers(response,IPP_TAG_PRINTER,IPP_TAG_ENUM,key,6,
           (const ipp_op_t[]){IPP_OP_PRINT_JOB,IPP_OP_VALIDATE_JOB,IPP_OP_CANCEL_JOB,
           IPP_OP_GET_JOB_ATTRIBUTES,IPP_OP_GET_JOBS,IPP_OP_GET_PRINTER_ATTRIBUTES});
        else if(strcmp(name,key="print-color-mode-supported")==0)
          ippAddString(response,IPP_TAG_PRINTER,IPP_TAG_KEYWORD,key,NULL,"auto");
        else if(strcmp(name,key="print-quality-supported")==0)
          ippAddInteger(response,IPP_TAG_PRINTER,IPP_TAG_ENUM,key,IPP_QUALITY_HIGH);
        else if(strcmp(name,key="printer-dns-sd-name")==0
         || strcmp(name,key="printer-name")==0)
          ippAddString(response,IPP_TAG_PRINTER,IPP_TAG_NAME,key,NULL,"PDF Printer");
        else if(strcmp(name,key="printer-is-accepting-jobs")==0)
          ippAddBoolean(response,IPP_TAG_PRINTER,key,true);
        else if(strcmp(name,key="sides-supported")==0)
          ippAddString(response,IPP_TAG_PRINTER,IPP_TAG_KEYWORD,key,NULL,"one-sided");
        else if(strcmp(name,key="urf-supported")==0)
          ippAddString(response,IPP_TAG_PRINTER,IPP_TAG_KEYWORD,key,NULL,"DM3");
      }
    }
    else if(operation!=IPP_OP_VALIDATE_JOB && operation!=IPP_OP_CANCEL_JOB
     && operation!=IPP_OP_GET_JOB_ATTRIBUTES && operation!=IPP_OP_GET_JOBS)
      ippSetStatusCode(response,IPP_STATUS_ERROR_OPERATION_NOT_SUPPORTED);
    transaction->response=response;
    return F_writeResponse(transaction,200,"application/ipp",ippLength(response));
  }
  return F_processIPPRequest(transaction);
}

static __attribute__((constructor)) void init() {
  MSImageRef image=MSGetImageByName("/System/Library/Frameworks/MobileCoreServices.framework/MobileCoreServices");
  _LSStartOpenOperation=MSFindSymbol(image,"__LSStartOpenOperation");
  _LSFinishOpenOperation=MSFindSymbol(image,"__LSFinishOpenOperation");
  intptr_t slide=_dyld_get_image_vmaddr_slide(0);
  MSHookFunction((void*)(0x100002388+slide),f_writeResponse,(void*)&F_writeResponse);
  MSHookFunction((void*)(0x1000027fc+slide),f_processIPPRequest,(void*)&F_processIPPRequest);
}
