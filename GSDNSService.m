#include <netdb.h>
#include "GSDNSService.h"

static void handleResolveReply(DNSServiceRef sdRef,DNSServiceFlags flags,uint32_t interface,DNSServiceErrorType ecode,const char* fullname,const char* host,uint16_t nport,uint16_t TXTlen,const unsigned char* TXT,void* context) {
  [(GSDNSService*)context handleResolveReply:ecode host:host nport:nport TXTlen:TXTlen TXT:TXT];
}

@implementation GSDNSService
@synthesize displayName=_displayName;
@synthesize displayText=_displayText;
@synthesize fullname=_fullname;
@synthesize TXTRecord=_TXTRecord;
@synthesize URL=_URL;
-(id)initWithInterface:(uint32_t)interface name:(const char*)name regtype:(const char*)regtype domain:(const char*)domain sdMaster:(DNSServiceRef)sdMaster {
  if((self=[super init])){
    _displayName=[[NSString alloc] initWithUTF8String:name];
    if(regtype && *regtype=='_'){
      const char* ptr=regtype+1;
      const char* end=strchr(ptr,'.');
      if(end){_scheme=[[NSString alloc] initWithBytes:ptr
       length:end-ptr encoding:NSUTF8StringEncoding];}
    }
    DNSServiceConstructFullName(
     _fullname=malloc(kDNSServiceMaxDomainName),name,regtype,domain);
    DNSServiceErrorType ecode=DNSServiceResolve(&sdMaster,kDNSServiceFlagsShareConnection,
     interface,name,regtype,domain,handleResolveReply,self);
    if(ecode==kDNSServiceErr_NoError){_sdTask=sdMaster;}
    else {_displayText=[[NSString alloc] initWithFormat:@"DNSServiceResolve: %d",ecode];}
  }
  return self;
}
-(id)initWithAddress:(NSString*)address defaultScheme:(NSString*)scheme {
  if((self=[super init])){
    NSURLComponents* components=[NSURLComponents componentsWithString:address];
    if(components.path && !components.host){
      components=[NSURLComponents componentsWithString:
       [@"//" stringByAppendingString:address]];
    }
    NSString* urlScheme=components.scheme;
    if(!urlScheme){urlScheme=components.scheme=scheme;}
    NSString* urlHost=components.host;
    if(!urlHost.length){urlHost=components.host=@"localhost";}
    NSNumber* urlPort=components.port;
    if(!urlPort){
      struct servent* ent=getservbyname(urlScheme.UTF8String,"tcp");
      if(ent){components.port=[NSNumber numberWithUnsignedShort:ntohs(ent->s_port)];}
    }
    NSURL* URL=components.URL;
    if(!URL){
      [self release];
      return nil;
    }
    _displayName=[[NSString alloc] initWithFormat:@"<%@>",urlHost];
    _displayText=[(_URL=[URL retain]).absoluteString retain];
  }
  return self;
}
-(void)handleResolveReply:(DNSServiceErrorType)ecode host:(const char*)host nport:(uint16_t)nport TXTlen:(uint16_t)TXTlen TXT:(const uint8_t*)TXT {
  [_displayText release];
  if(ecode==kDNSServiceErr_NoError){
    [_URL release];
    const NSStringEncoding encoding=NSUTF8StringEncoding;
    NSURLComponents* components=[[NSURLComponents alloc] init];
    components.scheme=_scheme;
    [components.host=[[NSString alloc] initWithCString:host encoding:encoding] release];
    components.port=[NSNumber numberWithUnsignedShort:ntohs(nport)];
    _displayText=[(_URL=[components.URL retain]).absoluteString retain];
    [components release];
    if(TXTlen){
      int index=0;
      char key[256];
      uint8_t valuelen;
      const uint8_t* value;
      NSMutableDictionary* TXTRecord=[[NSMutableDictionary alloc] init];
      _TXTRecord=TXTRecord;
      while(TXTRecordGetItemAtIndex(TXTlen,TXT,index++,
       sizeof(key),key,&valuelen,(const void**)&value)==kDNSServiceErr_NoError){
        NSString* keyString=[[NSString alloc] initWithCString:key encoding:encoding];
        NSString* valueString=[[NSString alloc]
         initWithBytes:value length:valuelen encoding:encoding];
        [TXTRecord setObject:valueString forKey:keyString];
        [keyString release];
        [valueString release];
      }
    }
  }
  else {_displayText=[[NSString alloc] initWithFormat:@"DNSServiceResolveReply: %d",ecode];}
  if(_sdTask){DNSServiceRefDeallocate(_sdTask);_sdTask=NULL;}
  [[NSNotificationCenter defaultCenter]
   postNotificationName:GSDNSServiceDidUpdateNotificationName object:self];
}
-(void)dealloc {
  [_displayName release];
  [_displayText release];
  if(_fullname){free(_fullname);}
  [_scheme release];
  [_TXTRecord release];
  [_URL release];
  if(_sdTask){DNSServiceRefDeallocate(_sdTask);}
  [super dealloc];
}
@end
