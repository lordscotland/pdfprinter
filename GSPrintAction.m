#include "GSPrintAction.h"
#include "GSPrintController.h"

@interface UIPrintActivity : UIActivity
-(UIImage*)_activityImage;
@end

static BOOL validActivityItems(NSArray* itemArray,NSArray** arrayptr) {
  NSURL** itembuf;
  NSURL** itemptr;
  if(arrayptr){itemptr=itembuf=malloc(itemArray.count*sizeof(NSURL*));}
  const CFTypeID tCFURL=CFURLGetTypeID();
  for (NSURL* itemURL in itemArray){
    if(CFGetTypeID(itemURL)!=tCFURL){continue;}
    CGPDFDocumentRef pdf=CGPDFDocumentCreateWithURL((CFURLRef)itemURL);
    if(pdf){
      CGPDFDocumentRelease(pdf);
      if(!arrayptr){return YES;}
      *itemptr++=itemURL;
    }
  }
  if(!arrayptr){return NO;}
  BOOL isValid=itemptr>itembuf;
  if(isValid){*arrayptr=[NSArray arrayWithObjects:itembuf count:itemptr-itembuf];}
  free(itembuf);
  return isValid;
}

@implementation GSPrintAction
@synthesize _activityImage;
@synthesize activityTitle=_activityTitle;
@synthesize activityViewController=_activityViewController;
-(id)init {
  if((self=[super init])){
    UIPrintActivity* activity=[[UIPrintActivity alloc] init];
    _activityImage=[activity._activityImage retain];
    _activityTitle=[activity.activityTitle retain];
    [activity release];
  }
  return self;
}
-(NSString*)activityType {
  return @"com.officialscheduler.GSPrint.print";
}
-(BOOL)canPerformWithActivityItems:(NSArray*)activityItems {
  return validActivityItems(activityItems,NULL);
}
-(void)prepareWithActivityItems:(NSArray*)activityItems {
  [_activityViewController release];
  if(validActivityItems(activityItems,&activityItems)){
    GSPrintController* controller=[[GSPrintController alloc]
     initWithActivityItems:activityItems activity:self];
    _activityViewController=[[UINavigationController alloc]
     initWithRootViewController:controller];
    [controller release];
  }
  else {_activityViewController=nil;}
}
-(void)activityDidFinish:(BOOL)completed {
  [super activityDidFinish:completed];
  [_activityViewController release];
  _activityViewController=nil;
}
-(void)dealloc {
  [_activityImage release];
  [_activityTitle release];
  [_activityViewController release];
  [super dealloc];
}
@end
