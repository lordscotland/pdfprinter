#include <dns_sd.h>

#define GSDNSServiceDidUpdateNotificationName @"GSDNSServiceDidUpdateNotification"

@interface GSDNSService : NSObject {
  NSString* _scheme;
  DNSServiceRef _sdTask;
}
@property(readonly,nonatomic) NSString* displayName;
@property(readonly,nonatomic) NSString* displayText;
@property(readonly,nonatomic) char* fullname;
@property(readonly,nonatomic) NSDictionary* TXTRecord;
@property(readonly,nonatomic) NSURL* URL;
-(id)initWithInterface:(uint32_t)interface name:(const char*)name regtype:(const char*)regtype domain:(const char*)domain sdMaster:(DNSServiceRef)sdMaster;
-(id)initWithAddress:(NSString*)address defaultScheme:(NSString*)scheme;
-(void)handleResolveReply:(DNSServiceErrorType)ecode host:(const char*)host nport:(uint16_t)nport TXTlen:(uint16_t)TXTlen TXT:(const unsigned char*)TXT;
@end
