#include <cups/cups.h>
#include <objc/runtime.h>
#include "GSPrintController.h"
#include "iapi.h"

#define GSOptionsLastDriverKey @"LastDriver"
#define GSOptionsLastDuplexModeKey @"LastDuplexMode"
#define GSOptionsLastPrinterAddressKey @"LastPrinterAddress"
#define GSOptionsLastPrinterNameKey @"LastPrinterName"
#define GSOptionsLastResolutionKey @"LastResolution"
#define GSOptionsPDLMapKey @"PDLMap"
#define GSOptionsDriverMapKey @"DriverMap"

enum GSOptionsSection {
  kGSOptionsSectionDevice,
  kGSOptionsSectionDocument,
  kGSOptionsSectionSubmit,
  kGSOptionsSectionLog,
  nGSOptionsSections
};
enum GSDeviceOption {
  kGSDeviceOptionService,
  kGSDeviceOptionDriver,
  kGSDeviceOptionResolution,
  kGSDeviceOptionDuplexMode,
  nGSDeviceOptions
};
static NSString* const GSDeviceOptionTitles[nGSDeviceOptions]={
  @"Printer",
  @"Driver",
  @"Resolution (dpi)",
  @"Duplex Mode",
};
static NSString* const GSDuplexModeTitles[nGSDuplexModes]={
  @"Off",
  @"Automatic (long edge)",
  @"Automatic (short edge)",
  @"Manual",
};
static NSString* const kGSItemCustom=@"Custom...";
static NSString* const kGSDriverDefault=@"(none)";
static NSString* const kGSResolutionDefault=@"Default";

static NSArray* GSDriverLists;
static NSOrderedSet* GSDriverListTitles;

static inline NSString* getDriverTitle(NSString* value) {
  return [value substringToIndex:1].uppercaseString;
}
struct gsWriteDeviceNames_context {
  NSMutableDictionary* lists;
  NSMutableArray* lastList;
}; 
static int gsWriteDeviceNames(void* gsc,const char* ptr,int len) {
  struct gsWriteDeviceNames_context* context=gsc;
  NSMutableDictionary* lists=context->lists;
  NSMutableArray* lastList=context->lastList;
  const char* const end=ptr+len;
  while(ptr<end){
    if(*ptr<=' '){ptr++;}
    else {
      const char* lineend=memchr(ptr,'\n',end-ptr);
      if(!lineend){lineend=end;}
      NSString* item=[[NSString alloc] initWithBytes:ptr
       length:lineend-ptr encoding:NSUTF8StringEncoding];
      if(lastList){
        NSUInteger index=lastList.count-1;
        [lastList replaceObjectAtIndex:index withObject:
         [[lastList objectAtIndex:index] stringByAppendingString:item]];
        lastList=nil;
      }
      else {
        NSString* title=getDriverTitle(item);
        NSMutableArray* array=[lists objectForKey:title];
        if(!array){[lists setObject:array=[NSMutableArray array] forKey:title];}
        [array addObject:item];
        if(lineend==end){lastList=array;}
      }
      [item release];
      ptr=lineend+1;
    }
  }
  context->lastList=lastList;
  return len;
}
static NSURL* getFullURL(GSDNSService* service) {
  if(!service){return nil;}
  NSURL* URL=service.URL;
  NSDictionary* TXTRecord;
  NSString* path;
  return (!URL.path.length && (TXTRecord=service.TXTRecord)
   && (path=[TXTRecord objectForKey:@"rp"]))?
   [URL URLByAppendingPathComponent:path]:URL;
}
static void setMapValue(NSUserDefaults* defaults,NSString* mkey,NSString* key,id value) {
  NSDictionary* map=[defaults dictionaryForKey:mkey];
  if(map){
    NSString* prev=[map objectForKey:key];
    if(prev?[prev isEqual:value]:!value){return;}
    NSMutableDictionary* mapM=[map.mutableCopy autorelease];
    if(value){[mapM setObject:value forKey:key];}
    else {[mapM removeObjectForKey:key];}
    map=mapM;
  }
  else {
    if(!value){return;}
    map=[NSDictionary dictionaryWithObject:value forKey:key];
  }
  [defaults setObject:map forKey:mkey];
}
static void updateServiceCell(UITableView* view,NSIndexPath* ipath,GSDNSService* service) {
  UITableViewCell* cell=[view cellForRowAtIndexPath:ipath];
  if(cell){cell.detailTextLabel.text=service.displayText;}
}
static ipp_t* newIPPRequest(ipp_op_t op,const char* uri,const char* user) {
  ipp_t* request=ippNewRequest(op);
  ippSetVersion(request,1,0);
  ippAddString(request,IPP_TAG_OPERATION,IPP_TAG_URI,
   "printer-uri",NULL,(char*)uri);
  ippAddString(request,IPP_TAG_OPERATION,IPP_TAG_NAME,
   "requesting-user-name",NULL,(char*)user);
  return request;
}
static size_t cgWriteRequestData(void* info,const void* buf,size_t len) {
  return (cupsWriteRequestData((http_t*)info,buf,len)==HTTP_CONTINUE)?len:0;
}
static void streamCopyRequestData(http_t* http,int fd) {
  ssize_t len;
  char buf[1<<12];
  while((len=read(fd,buf,sizeof(buf)))>0
   && cupsWriteRequestData(http,buf,len)==HTTP_CONTINUE);
}
static void showCUPSError(NSString* title) {
  NSString* message=[[NSString alloc]
   initWithCString:cupsLastErrorString() encoding:NSUTF8StringEncoding];
  dispatch_async(dispatch_get_main_queue(),^{
    UIAlertView* alert=[[UIAlertView alloc] initWithTitle:title message:message
     delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
  });
  [message release];
}
static void setResponseHandler(UIAlertView* alert,void(^handler)(NSInteger)) {
  void(^handlercopy)(NSInteger)=Block_copy(handler);
  objc_setAssociatedObject(alert,setResponseHandler,handlercopy,
   OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  Block_release(handlercopy);
}
static void handleBrowseReply(DNSServiceRef sdRef,DNSServiceFlags flags,uint32_t interface,DNSServiceErrorType ecode,const char* name,const char* regtype,const char* domain,void* context) {
  [(GSPrintController*)context handleBrowseReply:ecode flags:flags interface:interface name:name regtype:regtype domain:domain];
}

@implementation GSInputCell
@synthesize inputView=_inputView;
-(BOOL)canBecomeFirstResponder {
  return YES;
}
-(BOOL)becomeFirstResponder {
  const BOOL ok=[super becomeFirstResponder];
  if(ok){self.highlighted=YES;}
  return ok;
}
-(BOOL)resignFirstResponder {
  const BOOL ok=[super resignFirstResponder];
  if(ok){self.highlighted=NO;}
  return ok;
}
-(void)dealloc {
  [_inputView release];
  [super dealloc];
}
@end

@implementation GSPrintController
+(void)alertView:(UIAlertView*)alert clickedButtonAtIndex:(NSInteger)index {
  void(^handler)(NSInteger)=objc_getAssociatedObject(alert,setResponseHandler);
  handler(index);
}
-(id)initWithActivityItems:(NSArray*)activityItems activity:(UIActivity*)activity {
  if((self=[super initWithStyle:UITableViewStyleGrouped])){
    _activity=activity;
    _activityItems=[activityItems retain];
    _logMessages=[[NSMutableArray alloc] init];
    UITableViewCell* copiesCell=_numCopiesCell=[[UITableViewCell alloc]
     initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    copiesCell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIStepper* stepper=[[UIStepper alloc] init];
    stepper.minimumValue=1;
    stepper.maximumValue=INT_MAX;
    [stepper addTarget:self action:@selector(handleValueChanged:)
     forControlEvents:UIControlEventValueChanged];
    [self handleValueChanged:stepper];
    [copiesCell.accessoryView=stepper release];
    if(activityItems.count==1){
      NSURL* itemURL=[activityItems objectAtIndex:0];
      CGPDFDocumentRef pdf=CGPDFDocumentCreateWithURL((CFURLRef)itemURL);
      const size_t pageCount=CGPDFDocumentGetNumberOfPages(pdf);
      CGPDFDocumentRelease(pdf);
      if(pageCount>1){
        _pageCount=pageCount;
        GSInputCell* rangeCell=_rangeCell=[[GSInputCell alloc]
         initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        rangeCell.textLabel.text=@"Range";
        CGRect frame=rangeCell.frame;
        frame.size.height=250;
        UIPickerView* picker=[[UIPickerView alloc] initWithFrame:frame];
        picker.dataSource=self;
        picker.delegate=self;
        [picker selectRow:0 inComponent:0 animated:NO];
        [picker selectRow:pageCount-1 inComponent:1 animated:NO];
        [self pickerView:picker didSelectRow:0 inComponent:0];
        [rangeCell.inputView=picker release];
      }
    }
    _duplexModeLists=[[NSArray alloc] initWithObjects:
     @[GSDuplexModeTitles[kGSDuplexModeOff]],
     @[GSDuplexModeTitles[kGSDuplexModeAutoLongEdge],
     GSDuplexModeTitles[kGSDuplexModeAutoShortEdge]],
     @[GSDuplexModeTitles[kGSDuplexModeManual]],nil];
    _resolutionLists=[[NSArray alloc] initWithObjects:
     @[kGSResolutionDefault],@[kGSItemCustom],_resolutions=[NSOrderedSet
     orderedSetWithObjects:@"75",@"150",@"300",@"600",@"1200x600",nil],nil];
    _serviceLists=[[NSArray alloc] initWithObjects:
     @[kGSItemCustom],_services=[NSMutableArray array],nil];
    static dispatch_once_t once;
    dispatch_once(&once,^{
      void* gs;
      int ecode;
      NSMutableDictionary* lists=[[NSMutableDictionary alloc] init];
      gsapi_new_instance(&gs,&(struct gsWriteDeviceNames_context){.lists=lists});
      gsapi_set_stdio(gs,NULL,gsWriteDeviceNames,NULL);
      gsapi_init_with_args(gs,4,(char*[]){NULL,"-dNOPAUSE","-dQUIET","-dNODISPLAY"});
      gsapi_run_string(gs,"devicenames {=} forall",0,&ecode);
      gsapi_exit(gs);
      gsapi_delete_instance(gs);
      if(ecode){[self log:@"GS>devicenames: %d",ecode];}
      NSString* const blankTitle=@"";
      [lists setObject:@[kGSDriverDefault] forKey:blankTitle];
      NSOrderedSet* titles=GSDriverListTitles=[[NSOrderedSet alloc]
       initWithArray:[lists.allKeys sortedArrayUsingSelector:@selector(compare:)]];
      const NSUInteger ntitles=titles.count;
      NSOrderedSet** listbuf=malloc(ntitles*sizeof(NSOrderedSet*));
      NSOrderedSet** listptr=listbuf;
      for (NSString* title in titles){
        id driverList=[lists objectForKey:title];
        if(title!=blankTitle){
          [(NSMutableArray*)driverList sortUsingSelector:@selector(compare:)];
          driverList=[NSOrderedSet orderedSetWithArray:driverList];
        }
        *listptr++=driverList;
      }
      GSDriverLists=[[NSArray alloc] initWithObjects:listbuf count:ntitles];
      [lists release];
      free(listbuf);
    }); 
    NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
    NSString* value;
    if((value=[defaults stringForKey:GSOptionsLastPrinterAddressKey]))
      [self setItem:nil value:value forDeviceOption:kGSDeviceOptionService];
    else if((value=[defaults stringForKey:GSOptionsLastPrinterNameKey])){
      char* matchname=malloc(kDNSServiceMaxDomainName);
      if([value getCString:matchname maxLength:kDNSServiceMaxDomainName
       encoding:NSUTF8StringEncoding]){_matchname=matchname;}
      else {free(matchname);}
    }
    if((value=[defaults objectForKey:GSOptionsLastDriverKey]))
      [self setItem:nil value:value forDeviceOption:kGSDeviceOptionDriver];
    if((value=[defaults objectForKey:GSOptionsLastResolutionKey]))
      [self setItem:nil value:value forDeviceOption:kGSDeviceOptionResolution];
    if((value=[defaults objectForKey:GSOptionsLastDuplexModeKey]))
      [self setItem:nil value:value forDeviceOption:kGSDeviceOptionDuplexMode];
    self.title=@"Print Options";
    UINavigationItem* navitem=self.navigationItem;
    [navitem.leftBarButtonItem=[[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
     target:self action:@selector(dismiss)] release];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(serviceDidUpdate:)
     name:GSDNSServiceDidUpdateNotificationName object:nil];
    [self startBrowsing];
  }
  return self;
}
-(void)log:(NSString *)format,... {
  va_list args;
  va_start(args,format);
  NSString* message=[[NSString alloc] initWithFormat:format arguments:args];
  va_end(args);
  NSMutableArray* logMessages=_logMessages;
  const NSUInteger index=0;
  [logMessages insertObject:message atIndex:index];
  [message release];
  [self.tableView insertRowsAtIndexPaths:
   @[[NSIndexPath indexPathForRow:index inSection:kGSOptionsSectionLog]]
   withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)setItem:(id)item value:(id)value forDeviceOption:(enum GSDeviceOption)option {
  const BOOL preload=!self.isViewLoaded;
  NSString* displayName;
  switch(option){
    case kGSDeviceOptionService:{
      [_customService release];
      _customService=value?(item=[[GSDNSService alloc]
       initWithAddress:value defaultScheme:@"ipp"]):nil;
      GSDNSService* service=_selectedService=item;
      if(preload){return;}
      displayName=service.displayName;
      if(_matchname){free(_matchname);_matchname=NULL;}
      NSDictionary* TXTRecord=service.TXTRecord;
      if(!TXTRecord){break;}
      if((value=[TXTRecord objectForKey:@"pdl"])){
        NSDictionary* pdlMap=[[NSUserDefaults standardUserDefaults]
         dictionaryForKey:GSOptionsPDLMapKey];
        if(pdlMap && (value=[pdlMap objectForKey:value]))
          [self setItem:nil value:value forDeviceOption:kGSDeviceOptionDriver];
      }
      if((value=[TXTRecord objectForKey:@"Duplex"])
       && [((NSString*)value).uppercaseString isEqualToString:@"F"]){
        const enum GSDuplexMode mode=_duplexMode;
        if(mode==kGSDuplexModeAutoLongEdge || mode==kGSDuplexModeAutoShortEdge)
          [self setItem:nil value:[NSNumber numberWithInt:kGSDuplexModeManual]
           forDeviceOption:kGSDeviceOptionDuplexMode];
      }
      break;
    }
    case kGSDeviceOptionDriver:{
      if(value){
        NSUInteger index=[GSDriverListTitles indexOfObject:getDriverTitle(value)];
        item=(index==NSNotFound)?nil:[((NSOrderedSet*)[GSDriverLists
         objectAtIndex:index]).set member:value];
      }
      else if(item==kGSDriverDefault){item=nil;}
      _selectedDriver=item;
      if(preload){return;}
      displayName=item?:kGSDriverDefault;
      if(item){
        NSDictionary* driverMap=[[NSUserDefaults standardUserDefaults]
         dictionaryForKey:GSOptionsDriverMapKey];
        if(driverMap && (value=[driverMap objectForKey:item]))
          [self setItem:nil value:value forDeviceOption:kGSDeviceOptionResolution];
      }
      break;
    }
    case kGSDeviceOptionResolution:{
      if(value){
        if((item=[_resolutions.set member:value])){value=nil;}
        else {item=value=[value retain];}
      }
      else if(item==kGSResolutionDefault){item=nil;}
      [_customResolution release];
      _customResolution=value;
      _selectedResolution=item;
      if(preload){return;}
      displayName=item?:kGSResolutionDefault;
      break;
    }
    case kGSDeviceOptionDuplexMode:{
      enum GSDuplexMode mode=kGSDuplexModeOff;
      if(value){
        int i=((NSNumber*)value).intValue;
        if(i>=0 && i<nGSDuplexModes){mode=i;}
      }
      else {
        for (int i=0;i<nGSDuplexModes;i++){
          if(item==GSDuplexModeTitles[i]){mode=i;break;}
        }
      }
      _duplexMode=mode;
      if(preload){return;}
      displayName=GSDuplexModeTitles[mode];
      break;
    }
    default:return;
  }
  UITableViewCell* cell=[self.tableView cellForRowAtIndexPath:
   [NSIndexPath indexPathForRow:option inSection:kGSOptionsSectionDevice]];
  if(cell){cell.detailTextLabel.text=displayName;}
}
-(void)startBrowsing {
  DNSServiceRef sdMaster,sdTask;
  DNSServiceErrorType ecode;
  if((ecode=DNSServiceCreateConnection(&sdMaster))==kDNSServiceErr_NoError){
    sdTask=sdMaster;
    if((ecode=DNSServiceBrowse(&sdTask,kDNSServiceFlagsShareConnection,
     kDNSServiceInterfaceIndexAny,"_ipp._tcp",NULL,handleBrowseReply,self))
     !=kDNSServiceErr_NoError){[self log:@"DNSServiceBrowse(ipp): %d",ecode];}
    sdTask=sdMaster;
    if((ecode=DNSServiceBrowse(&sdTask,kDNSServiceFlagsShareConnection,
     kDNSServiceInterfaceIndexAny,"_ipps._tcp",NULL,handleBrowseReply,self))
     !=kDNSServiceErr_NoError){[self log:@"DNSServiceBrowse(ipps): %d",ecode];}
    DNSServiceSetDispatchQueue(_sdMaster=sdMaster,dispatch_get_main_queue());
  }
  else {[self log:@"DNSServiceCreateConnection: %d",ecode];}
}
-(void)handleBrowseReply:(DNSServiceErrorType)ecode flags:(DNSServiceFlags)flags interface:(uint32_t)interface name:(const char*)name regtype:(const char*)regtype domain:(const char*)domain {
  if(ecode==kDNSServiceErr_NoError){
    if(flags&kDNSServiceFlagsAdd){
      char fullname[kDNSServiceMaxDomainName];
      DNSServiceConstructFullName(fullname,name,regtype,domain);
      NSMutableArray* services=_services;
      int match=0;
      NSUInteger index=0;
      for (GSDNSService* service in services){
        if(strcmp(fullname,service.fullname)==0){
          match=(service==_selectedService)?2:1;
          break;
        }
        index++;
      }
      GSDNSService* service=[[GSDNSService alloc] initWithInterface:interface
       name:name regtype:regtype domain:domain sdMaster:_sdMaster];
      GSListController* controller=_servicesController;
      const NSInteger section=1;
      if(match){
        match--;
        [services replaceObjectAtIndex:index withObject:service];
        if(controller)
          updateServiceCell(controller.tableView,[NSIndexPath
           indexPathForRow:index inSection:section],service);
      }
      else {
        char* matchname=_matchname;
        if(matchname && strcmp(fullname,matchname)==0){
          if(controller){free(matchname);_matchname=NULL;}
          else {match=1;}
        }
        [services insertObject:service atIndex:index];
        if(controller)
          [controller.tableView insertRowsAtIndexPaths:
           @[[NSIndexPath indexPathForRow:index inSection:section]]
           withRowAnimation:UITableViewRowAnimationAutomatic];
      }
      if(match)
        [self setItem:service value:nil
         forDeviceOption:kGSDeviceOptionService];
      [service release];
    }
  }
  else if(ecode==kDNSServiceErr_ServiceNotRunning){
    DNSServiceRefDeallocate(_sdMaster);
    _sdMaster=NULL;
    [self startBrowsing];
  }
  else {[self log:@"DNSServiceBrowseReply: %d",ecode];}
}
-(void)serviceDidUpdate:(NSNotification*)note {
  GSListController* controller=_servicesController;
  if(!controller){return;}
  GSDNSService* service=note.object;
  NSMutableArray* services=_services;
  UITableView* view=controller.tableView;
  for (NSIndexPath* ipath in view.indexPathsForVisibleRows){
    if(ipath.section==1 && [services objectAtIndex:ipath.row]==service)
      updateServiceCell(view,ipath,service);
  }
}
-(void)listController:(GSListController*)controller didSelectItem:(id)item {
  enum GSDeviceOption option=controller.tag;
  if(item==kGSItemCustom){
    UIKeyboardType fieldKeyboardType;
    NSString* fieldPlaceholder;
    NSString* fieldText;
    switch(option){
      case kGSDeviceOptionService:
        fieldKeyboardType=UIKeyboardTypeURL;
        fieldPlaceholder=@"[ipp://]hostname[:port][/path]";
        fieldText=getFullURL(_selectedService).absoluteString;
        break;
      case kGSDeviceOptionResolution:
        fieldKeyboardType=UIKeyboardTypeASCIICapable;
        fieldPlaceholder=@"N[xN]";
        fieldText=_selectedResolution;
        break;
      default:return;
    }
    UIAlertController* alert=[UIAlertController
     alertControllerWithTitle:GSDeviceOptionTitles[option]
     message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField* field){
      field.keyboardType=fieldKeyboardType;
      field.returnKeyType=UIReturnKeyDone;
      field.placeholder=fieldPlaceholder;
      field.text=fieldText;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
     style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
     style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
      UITextField* field=[alert.textFields objectAtIndex:0];
      [self setItem:nil value:field.text forDeviceOption:option];
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
  }
  else {[self setItem:item value:nil forDeviceOption:option];}
}
-(void)listControllerIsDeallocating:(GSListController*)controller {
  if(controller==_servicesController){_servicesController=nil;}
}
-(void)handleValueChanged:(UIStepper*)stepper {
  _numCopiesCell.textLabel.text=[NSString
   stringWithFormat:@"Copies: %d",_numCopies=stepper.value];
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)picker {
  return 2;
}
-(NSInteger)pickerView:(UIPickerView*)picker numberOfRowsInComponent:(NSInteger)component {
  return _pageCount;
}
-(NSString*)pickerView:(UIPickerView*)picker titleForRow:(NSInteger)row forComponent:(NSInteger)component {
  return [NSString stringWithFormat:component?@"\u25c2 Page %zd":@"Page %zd \u25b8",row+1];
}
-(void)pickerView:(UIPickerView*)picker didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
  size_t rowIndex[2];
  rowIndex[component]=row;
  component=!component;
  rowIndex[component]=[picker selectedRowInComponent:component];
  if(rowIndex[0]>rowIndex[1])
    [picker selectRow:rowIndex[component]=row inComponent:component animated:YES];
  size_t nFirstPage=_nFirstPage=rowIndex[0]+1;
  size_t nLastPage=_nLastPage=rowIndex[1]+1;
  _rangeCell.detailTextLabel.text=(nFirstPage==nLastPage)?
   [NSString stringWithFormat:@"Page %zd",nFirstPage]:
   (nFirstPage==1 && nLastPage==_pageCount)?@"All pages":
   [NSString stringWithFormat:@"Pages %zd to %zd",nFirstPage,nLastPage];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)view {
  return nGSOptionsSections;
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)section {
  switch(section){
    case kGSOptionsSectionDevice:return nGSDeviceOptions;
    case kGSOptionsSectionDocument:return _rangeCell?2:1;
    case kGSOptionsSectionSubmit:return 1;
    case kGSOptionsSectionLog:return _logMessages.count;
  }
  return 0;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  UITableViewCell* cell;
  switch(ipath.section){
    case kGSOptionsSectionDevice:{
      NSString* displayName;
      enum GSDeviceOption option=ipath.row;
      switch(option){
        case kGSDeviceOptionService:
          displayName=_selectedService.displayName;break;
        case kGSDeviceOptionDriver:
          displayName=_selectedDriver?:kGSDriverDefault;break;
        case kGSDeviceOptionResolution:
          displayName=_selectedResolution?:kGSResolutionDefault;break;
        case kGSDeviceOptionDuplexMode:
          displayName=GSDuplexModeTitles[_duplexMode];break;
        default:return nil;
      }
      if(!(cell=[view dequeueReusableCellWithIdentifier:@"ItemCell"])){
        cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
         reuseIdentifier:@"ItemCell"] autorelease];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
      }
      cell.textLabel.text=GSDeviceOptionTitles[option];
      cell.detailTextLabel.text=displayName;
      break;
    }
    case kGSOptionsSectionDocument:
      if(ipath.row || !(cell=_rangeCell)){cell=_numCopiesCell;}
      break;
    case kGSOptionsSectionSubmit:
      if(!(cell=[view dequeueReusableCellWithIdentifier:@"SubmitCell"])){
        cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
         reuseIdentifier:@"SubmitCell"] autorelease];
        UILabel* label=cell.textLabel;
        label.text=@"Print";
        label.textColor=label.tintColor;
        label.textAlignment=NSTextAlignmentCenter;
      }
      break;
    case kGSOptionsSectionLog:
      if(!(cell=[view dequeueReusableCellWithIdentifier:@"MessageCell"])){
        cell=[[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
         reuseIdentifier:@"MessageCell"] autorelease];
        cell.userInteractionEnabled=NO;
      }
      cell.textLabel.text=[_logMessages objectAtIndex:ipath.row];
      break;
    default:return nil;
  }
  return cell;
}
-(void)tableView:(UITableView*)view didSelectRowAtIndexPath:(NSIndexPath*)ipath {
  [view deselectRowAtIndexPath:ipath animated:YES];
  switch(ipath.section){
    case kGSOptionsSectionDevice:{
      enum GSDeviceOption option=ipath.row;
      GSListController* controller;
      switch(option){
        case kGSDeviceOptionService:
          controller=_servicesController=[[GSListController alloc]
           initWithStyle:UITableViewStyleGrouped
           cellStyle:UITableViewCellStyleSubtitle
           itemLists:_serviceLists listTitles:nil
           selectedItem:_customService?kGSItemCustom:_selectedService
           delegate:self];
          break;
        case kGSDeviceOptionDriver:
          controller=[[GSListController alloc]
           initWithStyle:UITableViewStylePlain
           cellStyle:UITableViewCellStyleDefault
           itemLists:GSDriverLists listTitles:GSDriverListTitles.array
           selectedItem:_selectedDriver?:kGSDriverDefault
           delegate:self];
          break;
        case kGSDeviceOptionResolution:
          controller=[[GSListController alloc]
           initWithStyle:UITableViewStyleGrouped
           cellStyle:UITableViewCellStyleDefault
           itemLists:_resolutionLists listTitles:nil
           selectedItem:_customResolution?kGSItemCustom:
           _selectedResolution?:kGSResolutionDefault
           delegate:self];
          break;
        case kGSDeviceOptionDuplexMode:
          controller=[[GSListController alloc]
           initWithStyle:UITableViewStyleGrouped
           cellStyle:UITableViewCellStyleDefault
           itemLists:_duplexModeLists listTitles:nil
           selectedItem:GSDuplexModeTitles[_duplexMode]
           delegate:self];
          break;
        default:return;
      }
      controller.title=GSDeviceOptionTitles[controller.tag=option];
      [self.navigationController pushViewController:controller animated:YES];
      [controller release];
      break;
    }
    case kGSOptionsSectionDocument:
      if(ipath.row==0){
        GSInputCell* rangeCell=_rangeCell;
        if(rangeCell.isFirstResponder){[rangeCell resignFirstResponder];}
        else {[rangeCell becomeFirstResponder];}
      }
      break;
    case kGSOptionsSectionSubmit:{
      NSArray* activityItems=_activityItems;
      const NSUInteger itemCount=activityItems.count;
      if(!itemCount){break;}
      GSDNSService* service=_selectedService;
      NSURL* destURL=getFullURL(service);
      if(!destURL){
        NSIndexPath* ipath=[NSIndexPath indexPathForRow:kGSDeviceOptionService
         inSection:kGSOptionsSectionDevice];
        [view selectRowAtIndexPath:ipath animated:YES
         scrollPosition:UITableViewScrollPositionTop];
        [view deselectRowAtIndexPath:ipath animated:YES];
        break;
      }
      size_t nFirstPage=_nFirstPage,nLastPage=_nLastPage;
      if(nFirstPage==1 && nLastPage==_pageCount){nFirstPage=0;}
      NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
      char* fullname=service.fullname;
      if(fullname){
        [defaults setObject:[NSString stringWithCString:fullname
         encoding:NSUTF8StringEncoding] forKey:GSOptionsLastPrinterNameKey];
        [defaults removeObjectForKey:GSOptionsLastPrinterAddressKey];
      }
      else {
        [defaults setObject:destURL.absoluteString
         forKey:GSOptionsLastPrinterAddressKey];
        [defaults removeObjectForKey:GSOptionsLastPrinterNameKey];
      }
      NSString* driver=_selectedDriver;
      NSString* resolution=_selectedResolution;
      if(driver){
        setMapValue(defaults,GSOptionsDriverMapKey,driver,resolution);
        [defaults setObject:driver forKey:GSOptionsLastDriverKey];
      }
      else {[defaults removeObjectForKey:GSOptionsLastDriverKey];}
      if(resolution){[defaults setObject:resolution forKey:GSOptionsLastResolutionKey];}
      else {[defaults removeObjectForKey:GSOptionsLastResolutionKey];}
      const enum GSDuplexMode mode=_duplexMode;
      if(mode){[defaults setInteger:mode forKey:GSOptionsLastDuplexModeKey];}
      else {[defaults removeObjectForKey:GSOptionsLastDuplexModeKey];}
      NSDictionary* TXTRecord=service.TXTRecord;
      NSString* pdl;
      if(TXTRecord && (pdl=[TXTRecord objectForKey:@"pdl"]))
        setMapValue(defaults,GSOptionsPDLMapKey,pdl,driver);
      const int numCopies=_numCopies;
      NSString* jobName=[NSString stringWithFormat:@"GSPrint: [%@]",
       [((NSURL*)[activityItems objectAtIndex:0]).lastPathComponent
       stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
      if(itemCount>1){jobName=[jobName stringByAppendingFormat:@", +%zu",itemCount-1];}
      static dispatch_queue_t queue=NULL;
      if(!queue){queue=dispatch_queue_create("GSPrint.printQueue",NULL);}
      dispatch_async(queue,^{
        http_t* http=httpConnect(destURL.host.UTF8String,destURL.port.intValue);
        if(!http){showCUPSError(@"E: httpConnect");return;}
        const char* const uri=destURL.absoluteString.UTF8String;
        const char* const user=cupsUser();
        const char* const jobname=jobName.UTF8String;
        const char* const sides=
         (mode==kGSDuplexModeAutoLongEdge)?"two-sided-long-edge":
         (mode==kGSDuplexModeAutoShortEdge)?"two-sided-short-edge":"one-sided";
        const bool mduplex=mode==kGSDuplexModeManual;
        do {
          int iduplex;
          if(mduplex){
            static int buttonIndex;
            dispatch_semaphore_t signal=dispatch_semaphore_create(0);
            dispatch_async(dispatch_get_main_queue(),^{
              UIAlertView* alert=[[UIAlertView alloc] initWithTitle:@"Manual Duplex"
               message:nil delegate:[GSPrintController class] cancelButtonTitle:@"Done"
               otherButtonTitles:@"Print front sides",@"Print back sides",nil];
              setResponseHandler(alert,^(NSInteger index){
                buttonIndex=index-1;
                dispatch_semaphore_signal(signal);
              });
              [alert show];
              [alert release];
            });
            dispatch_semaphore_wait(signal,DISPATCH_TIME_FOREVER);
            dispatch_release(signal);
            if(buttonIndex==-1){break;}
            iduplex=buttonIndex;
          }
          else {iduplex=0;}
          ipp_t* request=newIPPRequest(IPP_OP_CREATE_JOB,uri,user);
          ippAddString(request, IPP_TAG_OPERATION,IPP_TAG_NAME,"job-name",NULL,jobname);
          ippAddInteger(request,IPP_TAG_JOB,IPP_TAG_INTEGER,"copies",numCopies);
          ippAddString(request,IPP_TAG_JOB,IPP_TAG_KEYWORD,"sides",NULL,sides);
          ipp_t* response=cupsDoRequest(http,request,"/");
          ipp_attribute_t* attr=ippFindAttribute(response,"job-id",IPP_TAG_INTEGER);
          const int job_id=attr?ippGetInteger(attr,0):0;
          ippDelete(response);
          if(!job_id){showCUPSError(@"E: CreateJob");break;}
          NSUInteger nremain=itemCount;
          for (NSURL* itemURL in activityItems){
            ipp_t* request=newIPPRequest(IPP_OP_SEND_DOCUMENT,uri,user);
            ippAddInteger(request,IPP_TAG_OPERATION,IPP_TAG_INTEGER,"job-id",job_id);
            ippAddString(request,IPP_TAG_OPERATION,IPP_TAG_NAME,
             "document-name",NULL,itemURL.lastPathComponent.UTF8String);
            ippAddBoolean(request,IPP_TAG_OPERATION,"last-document",!--nremain);
            cupsSendRequest(http,request,"/",CUPS_LENGTH_VARIABLE);
            ippDelete(request);
            if(driver){
              char* argv[9];
              char** argptr=argv;
              *argptr++=NULL;
              *argptr++="-dNOPAUSE";
              *argptr++="-dQUIET";
              *argptr++="-dSAFER";
              *argptr++=(char*)[@"-sDEVICE=" stringByAppendingString:driver].UTF8String;
              const char* const prefix="-sOutputFile=";
              const size_t prelen=strlen(prefix);
              const char* const xtmp=[NSTemporaryDirectory()
               stringByAppendingPathComponent:@"gs-XXXXXX"].fileSystemRepresentation;
              char* sOutputFile=*argptr++=malloc(prelen+strlen(xtmp)+1);
              char* tmpfn=strcpy(memcpy(sOutputFile,prefix,prelen)+prelen,xtmp);
              int fd=mkstemp(tmpfn);
              if(nFirstPage){
                const char* const prefix="-sPageList=";
                NSMutableData* plBuffer=[NSMutableData
                 dataWithBytes:prefix length:strlen(prefix)];
                char buf[5*sizeof(size_t)+2];
                if(mduplex){
                  size_t n=nFirstPage+iduplex;
                  while(n<=nLastPage){
                    int len=sprintf(buf,"%ld",n);
                    if((n+=2)<=nLastPage){buf[len]=',';}
                    [plBuffer appendBytes:buf length:len+1];
                  }
                }
                else {
                  int len=sprintf(buf,"%ld-%ld",nFirstPage,nLastPage);
                  [plBuffer appendBytes:buf length:len+1];
                }
                *argptr++=plBuffer.mutableBytes;
              }
              else if(iduplex){*argptr++="-sPageList=even";}
              else if(mduplex){*argptr++="-sPageList=odd";}
              if(resolution)
                *argptr++=(char*)[@"-r" stringByAppendingString:resolution].UTF8String;
              *argptr++=(char*)itemURL.fileSystemRepresentation;
              void* gs;
              gsapi_new_instance(&gs,NULL);
              gsapi_init_with_args(gs,argptr-argv,argv);
              gsapi_exit(gs);
              gsapi_delete_instance(gs);
              streamCopyRequestData(http,fd);
              close(fd);
              unlink(tmpfn);
              free(sOutputFile);
            }
            else if(nFirstPage || mduplex){
              CGDataConsumerRef consumer=CGDataConsumerCreate(http,
               &(CGDataConsumerCallbacks){.putBytes=cgWriteRequestData});
              CGContextRef context=CGPDFContextCreate(consumer,NULL,NULL);
              CGDataConsumerRelease(consumer);
              CGPDFDocumentRef pdf=CGPDFDocumentCreateWithURL((CFURLRef)itemURL);
              const size_t nlast=nLastPage?:CGPDFDocumentGetNumberOfPages(pdf);
              const size_t nstep=mduplex?2:1;
              for (size_t n=(nFirstPage?:1)+iduplex;n<=nlast;n+=nstep){
                CGPDFPageRef page=CGPDFDocumentGetPage(pdf,n);
                if(page){
                  CGRect box=CGPDFPageGetBoxRect(page,kCGPDFMediaBox);
                  CGContextBeginPage(context,&box);
                  CGContextDrawPDFPage(context,page);
                  CGContextEndPage(context);
                }
              }
              CGPDFDocumentRelease(pdf);
              CGContextRelease(context);
            }
            else {
              int fd=open(itemURL.fileSystemRepresentation,O_RDONLY);
              streamCopyRequestData(http,fd);
              close(fd);
            }
            ippDelete(cupsGetResponse(http,"/"));
            ipp_status_t status=cupsLastError();
            if(status>=0x200){showCUPSError(@"E: SendDocument");break;}
            else if(status>=0x100){showCUPSError(@"W: SendDocument");}
          }
        } while(mduplex);
        httpClose(http);
      });
      [_activity activityDidFinish:YES];
      break;
    }
  }
}
-(void)dismiss {
  [_activity activityDidFinish:NO];
}
-(void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [_rangeCell resignFirstResponder];
}
-(void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [_activityItems release];
  [_logMessages release];
  [_numCopiesCell release];
  [_rangeCell release];
  [_duplexModeLists release];
  [_resolutionLists release];
  [_serviceLists release];
  [_customResolution release];
  [_customService release];
  if(_matchname){free(_matchname);}
  if(_sdMaster){DNSServiceRefDeallocate(_sdMaster);}
  [super dealloc];
}
@end
