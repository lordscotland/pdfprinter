#import "GSDNSService.h"
#import "GSListController.h"

@interface GSInputCell : UITableViewCell
@property(retain,nonatomic) UIView* inputView;
@end

@interface GSPrintController : UITableViewController <GSListControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate> {
  UIActivity* _activity;
  NSArray* _activityItems;
  NSMutableArray* _logMessages;
  UITableViewCell* _numCopiesCell;
  GSInputCell* _rangeCell;
  NSArray* _duplexModeLists;
  NSArray* _resolutionLists;
  NSArray* _serviceLists;
  NSString* _customResolution;
  GSDNSService* _customService;
  NSOrderedSet* _resolutions;
  NSMutableArray* _services;
  char* _matchname;
  DNSServiceRef _sdMaster;
  NSString* _selectedDriver;
  NSString* _selectedResolution;
  GSDNSService* _selectedService;
  GSListController* _servicesController;
  size_t _nFirstPage,_nLastPage,_pageCount;
  enum GSDuplexMode {
    kGSDuplexModeOff,
    kGSDuplexModeAutoLongEdge,
    kGSDuplexModeAutoShortEdge,
    kGSDuplexModeManual,
    nGSDuplexModes
  } _duplexMode;
  int _numCopies;
}
-(id)initWithActivityItems:(NSArray*)activityItems activity:(UIActivity*)activity;
-(void)handleBrowseReply:(DNSServiceErrorType)ecode flags:(DNSServiceFlags)flags interface:(uint32_t)interface name:(const char*)name regtype:(const char*)regtype domain:(const char*)domain;
@end
