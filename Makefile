ARCHS = arm64
TARGET = iphone:latest

include theos/makefiles/common.mk

TWEAK_NAME = PDFPrinter
PDFPrinter_FILES = Tweak.c
PDFPrinter_FRAMEWORKS = CoreFoundation CoreGraphics
PDFPrinter_PRIVATE_FRAMEWORKS = PrintKit

include $(THEOS_MAKE_PATH)/tweak.mk

APPLICATION_NAME = GSPrint
GSPrint_FILES = main.m $(wildcard GS*.m)
GSPrint_FRAMEWORKS = CoreGraphics QuickLook UIKit
GSPrint_PRIVATE_FRAMEWORKS = PrintKit
GSPrint_OBJ_FILES = gs.a
GSPrint_LDFLAGS = -liconv

include $(THEOS_MAKE_PATH)/application.mk
