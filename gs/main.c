#include <stdio.h>
#include "iapi.h"

int main(int argc,char** argv) {
  void* gs;
  gsapi_new_instance(&gs,NULL);
  gsapi_init_with_args(gs,argc,argv);
  gsapi_exit(gs);
  gsapi_delete_instance(gs);
}
