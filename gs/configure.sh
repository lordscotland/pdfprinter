#!/bin/sh

HOST=arm-apple-darwin
PREFIX=/opt/theos/toolchain/bin/$HOST-
CC=${PREFIX}clang
FLAGS='-isysroot /opt/theos/sdks/iPhoneOS8.1.sdk -arch arm64'
ARCH_H=$(cd $(dirname "$0");pwd)/arch.h

./configure --host=$HOST --with-arch-h="$ARCH_H" --without-libtiff --without-ijs\
 CC="$CC" CFLAGS="$FLAGS" CPPFLAGS='-DPNG_ARM_NEON_OPT=0'\
 LDFLAGS="$FLAGS" CCAUX=cc CFLAGSAUX=' ' LDFLAGSAUX=' '

sed -i~ -e "s!^\(AR\|RANLIB\)=!\1=$PREFIX!" Makefile

if [ ! -f "$ARCH_H" ]; then
  "$CC" $FLAGS -o genarch base/genarch.c && isign -f genarch
  echo '*** Run ./genarch arch.h on target platform'
fi
