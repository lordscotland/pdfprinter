#include "GSPrintAction.h"
#include "GSRootController.h"

@interface UIImage (Private)
+(UIImage*)kitImageNamed:(NSString*)name;
@end

@interface GS_UIViewController : UIViewController @end
@implementation GS_UIViewController
-(void)loadView {
  UIImageView* view=[[UIImageView alloc]
   initWithImage:[UIImage kitImageNamed:@"UIPrinterNoJobIcon"]];
  view.contentMode=UIViewContentModeCenter;
  view.backgroundColor=[UIColor groupTableViewBackgroundColor];
  [self.view=view release];
}
@end

@interface GS_QLPreviewController : QLPreviewController @end
@implementation GS_QLPreviewController
-(NSArray*)excludedActivityTypesForDocumentInteractionController:(UIDocumentInteractionController*)controller {
  return @[UIActivityTypePrint];
}
-(NSArray*)additionalActivitiesForDocumentInteractionController:(UIDocumentInteractionController*)controller {
  static dispatch_once_t once;
  static GSPrintAction* printAction;
  dispatch_once(&once,^{printAction=[[GSPrintAction alloc] init];});
  return @[printAction];
}
@end

@implementation GSRootController
-(id)init {
  if((self=[super init])){
    _directoryURL=[[[NSFileManager defaultManager]
     URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask
     appropriateForURL:nil create:NO error:NULL] retain];
    [self reloadItems];
  }
  return self;
}
-(void)reloadItems {
  NSFileManager* manager=[NSFileManager defaultManager];
  NSArray* URLs=[manager contentsOfDirectoryAtURL:_directoryURL
   includingPropertiesForKeys:nil options:0 error:NULL];
  NSURL** urlbuf=malloc(URLs.count*sizeof(*urlbuf));
  NSURL** urlptr=urlbuf;
  for (NSURL* URL in URLs){
    NSNumber* isFile;
    if([URL getResourceValue:&isFile forKey:NSURLIsRegularFileKey error:nil]
     && isFile.boolValue){*urlptr++=URL;}
  }
  UIViewController* controller;
  [_previewController release];
  [_previewURLs release];
  if(urlptr==urlbuf){
    _previewURLs=nil;
    _previewController=nil;
    controller=[[[GS_UIViewController alloc] init] autorelease];
    controller.title=[[NSBundle mainBundle]
     objectForInfoDictionaryKey:(id)kCFBundleNameKey];
    controller.hidesBottomBarWhenPushed=YES;
  }
  else {
    _previewURLs=[[NSArray alloc] initWithObjects:urlbuf count:urlptr-urlbuf];
    controller=_previewController=[[GS_QLPreviewController alloc] init];
    ((QLPreviewController*)controller).dataSource=self;
    [controller.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
     target:self action:@selector(removeCurrentPreviewItem)] release];
  }
  free(urlbuf);
  self.viewControllers=@[controller];
}
-(BOOL)handleURL:(NSURL*)URL {
  if(!URL.isFileURL){return NO;}
  unsigned long index=0;
  NSString* pathExtension;
  NSString* pathBase;
  NSURL* destURL=[_directoryURL URLByAppendingPathComponent:URL.lastPathComponent];
  while([_previewURLs containsObject:destURL]){
    if(!index){
      NSString* path=destURL.path;
      pathExtension=path.pathExtension;
      if(!pathExtension.length){pathExtension=nil;}
      pathBase=path.stringByDeletingPathExtension;
    }
    NSString* path=[pathBase stringByAppendingFormat:@"-%lu",++index];
    if(pathExtension){path=[path stringByAppendingPathExtension:pathExtension];}
    destURL=[NSURL fileURLWithPath:path];
  }
  return [[NSFileManager defaultManager]
   moveItemAtURL:URL toURL:destURL error:NULL];
}
-(void)removeCurrentPreviewItem {
  [[NSFileManager defaultManager] removeItemAtURL:[_previewURLs
   objectAtIndex:_previewController.currentPreviewItemIndex] error:NULL];
}
-(NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController*)controller {
  return (controller==_previewController)?_previewURLs.count:0;
}
-(id<QLPreviewItem>)previewController:(QLPreviewController*)controller previewItemAtIndex:(NSInteger)index {
  return (controller==_previewController)?[_previewURLs objectAtIndex:index]:nil;
}
-(void)viewDidLoad {
  int fd=open(_directoryURL.fileSystemRepresentation,O_EVTONLY);
  if(fd!=-1){
    UIView* view=self.view;
    UIActivityIndicatorView* indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:view.bounds];
    indicatorView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    indicatorView.backgroundColor=[UIColor colorWithWhite:0 alpha:0.5];
    indicatorView.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhiteLarge;
    [view addSubview:indicatorView];
    dispatch_queue_t queue=dispatch_get_main_queue();
    dispatch_source_t timer=dispatch_source_create(
     DISPATCH_SOURCE_TYPE_TIMER,0,0,queue);
    dispatch_source_set_event_handler(timer,^{
      [self reloadItems];
      [indicatorView stopAnimating];
    });
    [indicatorView release];
    dispatch_source_t source=_directorySource=dispatch_source_create(
     DISPATCH_SOURCE_TYPE_VNODE,fd,DISPATCH_VNODE_WRITE,queue);
    dispatch_source_set_event_handler(source,^{
      [indicatorView startAnimating];
      dispatch_source_set_timer(timer,
       dispatch_time(DISPATCH_TIME_NOW,NSEC_PER_SEC/5),
       DISPATCH_TIME_FOREVER,NSEC_PER_SEC/5);
    });
    dispatch_source_set_cancel_handler(source,^{
      dispatch_source_cancel(timer);
      close(fd);
    });
    dispatch_resume(source);
    dispatch_resume(timer);
    dispatch_release(timer);
  }
}
-(void)dealloc {
  dispatch_source_t source=_directorySource;
  if(source){
    dispatch_source_cancel(source);
    dispatch_release(source);
  }
  [_directoryURL release];
  [_previewController release];
  [_previewURLs release];
  [super dealloc];
}
@end
