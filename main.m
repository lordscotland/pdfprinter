#include "GSRootController.h"

@interface GSAppDelegate : NSObject <UIApplicationDelegate> {GSRootController* controller;}
@property(retain,nonatomic) UIWindow* window;
@end

@implementation GSAppDelegate
@synthesize window;
-(BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)options {
  window=[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  window.rootViewController=controller=[[GSRootController alloc] init];
  [window makeKeyAndVisible];
  NSURL* URL=[options objectForKey:UIApplicationLaunchOptionsURLKey];
  return URL?[controller handleURL:URL]:YES;
}
-(BOOL)application:(UIApplication*)application handleOpenURL:(NSURL*)URL {
  return URL?[controller handleURL:URL]:NO;
}
-(void)dealloc {
  [controller release];
  [window release];
  [super dealloc];
}
@end

int main(int argc,char** argv) {@autoreleasepool {
  return UIApplicationMain(argc,argv,nil,@"GSAppDelegate");
}}
