#include "GSListController.h"

@protocol CompositeItem <NSObject>
@property(readonly,nonatomic) NSString* displayName;
@property(readonly,nonatomic) NSString* displayText;
@end

static inline id itemForIndexPath(NSArray* itemLists,NSIndexPath* ipath) {
  return [[itemLists objectAtIndex:ipath.section] objectAtIndex:ipath.row];
}

@implementation GSListController
@synthesize tag=_tag;
-(id)initWithStyle:(UITableViewStyle)style cellStyle:(UITableViewCellStyle)cellStyle itemLists:(NSArray*)itemLists listTitles:(NSArray*)listTitles selectedItem:(id)selectedItem delegate:(id<GSListControllerDelegate>)delegate {
  if((self=[super initWithStyle:style])){
    _cellStyle=cellStyle;
    _delegate=delegate;
    _itemLists=[itemLists retain];
    _listTitles=[listTitles retain];
    _selectedItem=selectedItem;
  }
  return self;
}
-(void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  if(_viewWillAppear){return;}
  _viewWillAppear=YES;
  UITableView* view=self.tableView;
  if(_cellStyle==UITableViewCellStyleSubtitle){view.rowHeight=56;}
  id selectedItem=_selectedItem;
  NSUInteger section=0;
  for (NSArray* list in _itemLists){
    NSUInteger index=[list respondsToSelector:@selector(indexOfObjectIdenticalTo:)]?
     [list indexOfObjectIdenticalTo:selectedItem]:[list indexOfObject:selectedItem];
    if(index!=NSNotFound){
      [view scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:section]
       atScrollPosition:UITableViewScrollPositionTop animated:NO];
      break;
    }
    section++;
  }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView*)view {
  return _itemLists.count;
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)section {
  return ((NSArray*)[_itemLists objectAtIndex:section]).count;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  UITableViewCell* cell=[view dequeueReusableCellWithIdentifier:@"Cell"]?:
   [[[UITableViewCell alloc] initWithStyle:_cellStyle reuseIdentifier:@"Cell"] autorelease];
  id item=itemForIndexPath(_itemLists,ipath);
  cell.accessoryType=(item==_selectedItem)?
   UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
  if(![item isKindOfClass:[NSString class]]){
    id<CompositeItem> composite=item;
    cell.detailTextLabel.text=composite.displayText;
    item=composite.displayName;
  }
  cell.textLabel.text=item;
  return cell;
}
-(NSString*)tableView:(UITableView*)view titleForHeaderInSection:(NSInteger)section {
  return _listTitles?[_listTitles objectAtIndex:section]:nil;
}
-(NSArray*)sectionIndexTitlesForTableView:(UITableView*)view {
  return _listTitles;
}
-(void)tableView:(UITableView*)view didSelectRowAtIndexPath:(NSIndexPath*)ipath {
  [view deselectRowAtIndexPath:ipath animated:YES];
  [self.navigationController popViewControllerAnimated:YES];
  [_delegate listController:self didSelectItem:itemForIndexPath(_itemLists,ipath)];
}
-(void)dealloc {
  [_delegate listControllerIsDeallocating:self];
  [_itemLists release];
  [_listTitles release];
  [super dealloc];
}
@end
