@interface GSPrintAction : UIActivity
@property(readonly,nonatomic) UIImage* _activityImage;
@property(readonly,nonatomic) NSString* activityTitle;
@property(readonly,nonatomic) UIViewController* activityViewController;
@end
