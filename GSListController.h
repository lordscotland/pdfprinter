@class GSListController;

@protocol GSListControllerDelegate <NSObject>
-(void)listController:(GSListController*)controller didSelectItem:(id)item;
-(void)listControllerIsDeallocating:(GSListController*)controller;
@end

@interface GSListController : UITableViewController {
  UITableViewCellStyle _cellStyle;
  id<GSListControllerDelegate> _delegate;
  NSArray* _itemLists;
  NSArray* _listTitles;
  id _selectedItem;
  BOOL _viewWillAppear;
}
@property(assign,nonatomic) NSInteger tag;
-(id)initWithStyle:(UITableViewStyle)style cellStyle:(UITableViewCellStyle)cellStyle itemLists:(NSArray*)itemLists listTitles:(NSArray*)listTitles selectedItem:(id)selectedItem delegate:(id<GSListControllerDelegate>)delegate;
@end

